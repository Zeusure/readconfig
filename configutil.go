package readconfig

import (
	"bufio"
	"errors"
	"io"
	"log"
	"os"
	"runtime/debug"
	"strconv"
	"strings"
)

func OnErr(err error) {
	log.Println(err)
	debug.PrintStack()
}

type ConfigUtil struct {
	filePath string
	config   map[string]string
}

func ConfigUtilOf(filePath string) *ConfigUtil {
	return &ConfigUtil{filePath: filePath}
}

/**
 * @Author zeusure
 * @Description: 解析配置文件，逐行读取，格式为 key = value
 * @param filePath
 * @return map[string]interface{}
 */
func (this *ConfigUtil) ParseConfig() map[string]string {
	f, err := os.Open(this.filePath)
	defer f.Close()
	if err != nil {
		OnErr(err)
		return nil
	}
	buf := bufio.NewReader(f)
	this.config = make(map[string]string)
	for {
		line, err := buf.ReadString('\n')
		line = strings.TrimSpace(line)
		if strings.HasPrefix(line, "#") {
			continue
		}
		index := strings.Index(line, "=")
		k := line[0:index]
		v := line[index+1:]
		this.config[k] = v
		if err != nil {
			if err == io.EOF {
				break
			}
			OnErr(err)
			return nil
		}
	}
	return this.config
}

func (this *ConfigUtil) String(k string) string {
	if v, ok := this.config[k]; ok {
		return v
	}
	OnErr(errors.New("无此配置项 : " + k))
	return ""
}

func (this *ConfigUtil) StringDefault(k, d string) string {
	if v, ok := this.config[k]; ok {
		return v
	} else {
		return d
	}
}

func (this *ConfigUtil) Int(k string) int {
	if v, ok := this.config[k]; ok {
		s, err := strconv.Atoi(v)
		if err != nil {
			OnErr(errors.New("配置项 " + k + " 转换错误"))
		}
		return s
	}
	OnErr(errors.New("无此配置项 : " + k))
	return 0
}

func (this *ConfigUtil) IntDefault(k string, d int) int {
	if v, ok := this.config[k]; ok {
		s, err := strconv.Atoi(v)
		if err != nil {
			OnErr(errors.New("配置项 " + k + " 转换错误"))
		}
		return s
	} else {
		return d
	}
}

func (this *ConfigUtil) Float32(k string) float32 {
	if v, ok := this.config[k]; ok {
		s, err := strconv.ParseFloat(v, 32)
		if err != nil {
			OnErr(errors.New("配置项 " + k + " 转换错误"))
		}
		return float32(s)
	}
	OnErr(errors.New("无此配置项 : " + k))
	return 0
}

func (this *ConfigUtil) Float32Default(k string, d float32) float32 {
	if v, ok := this.config[k]; ok {
		s, err := strconv.ParseFloat(v, 32)
		if err != nil {
			OnErr(errors.New("配置项 " + k + " 转换错误"))
		}
		return float32(s)
	} else {
		return d
	}
}

func (this *ConfigUtil) Float64(k string) float64 {
	if v, ok := this.config[k]; ok {
		s, err := strconv.ParseFloat(v, 64)
		if err != nil {
			OnErr(errors.New("配置项 " + k + " 转换错误"))
		}
		return s
	}
	OnErr(errors.New("无此配置项 : " + k))
	return 0
}

func (this *ConfigUtil) Float64Default(k string, d float64) float64 {
	if v, ok := this.config[k]; ok {
		s, err := strconv.ParseFloat(v, 64)
		if err != nil {
			OnErr(errors.New("配置项 " + k + " 转换错误"))
		}
		return s
	} else {
		return d
	}
}

func (this *ConfigUtil) Bool(k string) bool {
	if v, ok := this.config[k]; ok {
		if "true" == v {
			return true
		} else if "false" == v {
			return false
		} else {
			OnErr(errors.New("配置项 " + k + " 转换错误"))
			return false
		}
	}
	OnErr(errors.New("无此配置项 : " + k))
	return false
}

func (this *ConfigUtil) BoolDefault(k string, d bool) bool {
	if v, ok := this.config[k]; ok {
		if "true" == v {
			return true
		} else if "false" == v {
			return false
		} else {
			OnErr(errors.New("配置项 " + k + " 转换错误"))
			return d
		}
	} else {
		return d
	}
}
