package readconfig

import (
	"testing"
)

var (
	name         string
	version      string
	developCount int
	fullCost     float64
)

func Test_ConfigUtil(t *testing.T) {
	c := ConfigUtilOf("dev.config")
	c.ParseConfig()
	name = c.String("name")
	t.Log("name value as followed:")
	t.Log(name)
	version = c.StringDefault("version", "v0.1")
	t.Log("version value as followed:")
	t.Log(version)
	developCount = c.Int("developer_count")
	t.Log("developer_count value as followed:")
	t.Log(developCount)
	e := c.Int("12")
	t.Log("e value as followed:")
	t.Log(e)
	fullCost = c.Float64("full_cost")
	t.Log("full_cost value as followed:")
	t.Log(fullCost)
}
